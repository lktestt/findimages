//
//  ImagesModel.swift
//  FindImagesApp
//
//  Created by Moskalenko on 8/30/19.
//  Copyright © 2019 LoraKucher. All rights reserved.
//

import UIKit

class ImagesList: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case photos
    }
    
    let photos: [Image]
    
    enum PhotosCodingKeys: String, CodingKey {
        case photo
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let ratingsContainer = try container.nestedContainer(keyedBy: PhotosCodingKeys.self, forKey: .photos)
        self.photos = try ratingsContainer.decode([Image].self, forKey: .photo)
    }
}

class Image: Decodable {
    
    var id: String
    
    enum CodingKeys: String, CodingKey {
        case id
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
    }
}

class Photos: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case sizes
    }
    
    let sizes: [OnePhoto]
    
    enum PhotosCodingKeys: String, CodingKey {
        case size
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let ratingsContainer = try container.nestedContainer(keyedBy: PhotosCodingKeys.self, forKey: .sizes)
        self.sizes = try ratingsContainer.decode([OnePhoto].self, forKey: .size)
    }
}

class OnePhoto: Decodable {
    
    var source: String
    
    enum CodingKeys: String, CodingKey {
        case source
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.source = try container.decode(String.self, forKey: .source)
    }
}
