//
//  ServiceHelper.swift
//  FindImagesApp
//
//  Created by Moskalenko on 8/30/19.
//  Copyright © 2019 LoraKucher. All rights reserved.
//

import UIKit

final class ServiceHelper {
    
    static func request(with url: URL, completion: @escaping (Data) -> Void) {
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let dataResponse = data, error == nil else {
                print(error?.localizedDescription ?? "Response Error")
                return
            }
            completion(dataResponse)
        }
        task.resume()
    }
}
