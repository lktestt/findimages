////
////  ImagesServiceAPI.swift
////  FindImagesApp
////
////  Created by Moskalenko on 8/30/19.
////  Copyright © 2019 LoraKucher. All rights reserved.
////

import UIKit

final class ImagesServiceAPI {

    public static let shared = ImagesServiceAPI()

    func getPhoto(with tag: String) {
        guard let url = URL(string: ServiceApiConstants.searchPhotos(with: tag)) else {
            preconditionFailure("url is nil")
        }
        ServiceHelper.request(with: url) { dataResponse in
            do {
                let parsedResult: ImagesList = try JSONDecoder().decode(ImagesList.self, from: dataResponse)
                guard let photoId = parsedResult.photos.first?.id else {
                    preconditionFailure("url is nil")
                }
                self.getImage(with: photoId)
            } catch let parsingError {
                print("Error", parsingError)
            }
        }
    }
    
    private func getImage(with id: String) {
        guard let url = URL(string: ServiceApiConstants.getPhoto(by: id)) else {
            preconditionFailure("url is nil")
        }
        ServiceHelper.request(with: url) { dataResponse in
            do {
                let parsedResult: Photos = try JSONDecoder().decode(Photos.self, from: dataResponse)
                guard let source = parsedResult.sizes.last?.source,
                    let sourceUrl = URL(string: source) else {
                        return
                }
                self.downloadImage(from: sourceUrl)
            } catch let parsingError {
                print("Error", parsingError)
            }
        }
    }
    
    private func downloadImage(from url: URL) {
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async() {
                ImageObject.add(data)
            }
        }
    }
    
    private func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
}
