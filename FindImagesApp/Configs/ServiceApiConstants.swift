//
//  ServiceApiConstants.swift
//  FindImagesApp
//
//  Created by Moskalenko on 8/30/19.
//  Copyright © 2019 LoraKucher. All rights reserved.
//

import UIKit

final class ServiceApiConstants {

    static func searchPhotos(with text: String) -> String {
        return "https://api.flickr.com/services/rest/?&method=flickr.photos.search&api_key=a2c711a7a20d02f4e1e5b1e2e23d798d&text=\(text)&format=json&nojsoncallback=1"
    }
    
    static func getPhoto(by id: String) -> String {
        return "https://api.flickr.com/services/rest/?&method=flickr.photos.getSizes&api_key=a2c711a7a20d02f4e1e5b1e2e23d798d&photo_id=\(id)&format=json&nojsoncallback=1"
    }
}
