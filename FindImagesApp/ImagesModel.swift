//
//  ImagesModel.swift
//  FindImagesApp
//
//  Created by Moskalenko on 8/30/19.
//  Copyright © 2019 LoraKucher. All rights reserved.
//

import UIKit
import RealmSwift

class ImageObject: Object {
    
    static let images = try! Realm().objects(ImageObject.self)
    
    @objc dynamic var image: Data?
    @objc private dynamic var id = Int(Date().timeIntervalSince1970)
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    static func add(_ newImage: Data) {
        let realm = try! Realm()
        
        try! realm.write {
            let newImageObject = ImageObject()
            newImageObject.image = newImage
            realm.add(newImageObject)
            NotificationCenter.default.post(name: Notification.Name("reload"), object: nil)

        }
    }
}
