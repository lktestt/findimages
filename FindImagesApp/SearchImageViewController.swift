//
//  ViewController.swift
//  FindImagesApp
//
//  Created by Moskalenko on 8/30/19.
//  Copyright © 2019 LoraKucher. All rights reserved.
//

import UIKit

class SearchImageViewController: UIViewController, UISearchBarDelegate {
    
    // MARK: - Private properties
    private let images = ImageObject.images
    private let space: CGFloat = 5
    private let cellSize = 300
    private let activityView = UIActivityIndicatorView(style: .whiteLarge)
    
    private var collectionView: UICollectionView!
    private lazy var searchBar: UISearchBar = UISearchBar()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addObserver()
        createCollectionView()
        createSearchBar()
    }
}

// MARK: - Private methods
extension SearchImageViewController {
    
    private func addObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadCollectionView(notification:)), name: Notification.Name("reload"), object: nil)
    }
    
    private func createCollectionView() {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.minimumInteritemSpacing = space
        flowLayout.minimumLineSpacing = space
        self.collectionView = UICollectionView(frame: self.view.bounds, collectionViewLayout: flowLayout)
        collectionView.register(ImageCell.self, forCellWithReuseIdentifier: "collectionCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .lightGray
        self.view.addSubview(collectionView)
    }
    
    private func createSearchBar() {
        searchBar.searchBarStyle = .prominent
        searchBar.placeholder = " Search..."
        searchBar.sizeToFit()
        searchBar.isTranslucent = false
        searchBar.backgroundImage = UIImage()
        searchBar.delegate = self
        navigationItem.titleView = searchBar
    }
    
    @objc private func reloadCollectionView(notification: Notification) {
        collectionView.reloadData()
        activityView.stopAnimating()
        activityView.removeFromSuperview()
        searchBar.endEditing(true)
    }
}

// MARK: - SearchBar methods
extension SearchImageViewController {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text else {
            return
        }
        activityView.center = view.center
        view.addSubview(activityView)
        activityView.startAnimating()
        ImagesServiceAPI.shared.getPhoto(with: text)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text?.removeAll()
        searchBar.endEditing(true)
    }
}

// MARK: - UICIllectionView methods
extension SearchImageViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath as IndexPath) as? ImageCell ?? ImageCell()

        cell.imageView.image = UIImage(data: Array(images)[indexPath.row].image!)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cellSize, height: cellSize)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: space, left: space, bottom: space, right: space)
    }
}

